package no.noroff.IDP_Security_OAuth2.controllers;

import no.noroff.IDP_Security_OAuth2.models.User;
import no.noroff.IDP_Security_OAuth2.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.security.Principal;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;

@RestController
@RequestMapping("/user/info")
public class UserController {

    @Autowired
    private UserService userService;

    // This endpoint just shows the information from the token
    // The token is received through the @AuthenticationPrincipal via Spring Security.
    @GetMapping
    public Map<String, Object> getUserInfo(@AuthenticationPrincipal Jwt principal) {
        Map<String, String> map = new Hashtable<String, String>();
        map.put("user_name", principal.getClaimAsString("preferred_username"));
        map.put("email", principal.getClaimAsString("email"));
        map.put("first_name", principal.getClaimAsString("given_name"));
        map.put("last_name", principal.getClaimAsString("family_name"));
        map.put("roles", String.valueOf(principal.getClaimAsStringList("roles")));
        return Collections.unmodifiableMap(map);
    }

    // This lets us see the entire principal object that spring security keeps of our user
    @GetMapping("/principal")
    public Principal getUser(Principal user){
        return user;
    }

    @PostMapping
    public ResponseEntity<User> addNewUser(@AuthenticationPrincipal Jwt principal){
        if(userService.checkIfUserExists(principal.getClaimAsString("email")))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

        return ResponseEntity.ok(userService.createNewUserProfileFromJWT(principal));
    }
}
