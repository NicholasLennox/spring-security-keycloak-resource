package no.noroff.IDP_Security_OAuth2.models;

public enum RoleType {
    Administrator,
    User
}
