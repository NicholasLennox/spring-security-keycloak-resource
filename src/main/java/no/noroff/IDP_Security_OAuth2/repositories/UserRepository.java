package no.noroff.IDP_Security_OAuth2.repositories;

import no.noroff.IDP_Security_OAuth2.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByEmail(String email);
}
